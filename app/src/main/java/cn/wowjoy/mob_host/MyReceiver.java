package cn.wowjoy.mob_host;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if ("ACTION_UPDATE_PLUGIN".equals(intent.getAction()))
            Log.e("666", "" + intent.getStringExtra("url"));
    }
}
