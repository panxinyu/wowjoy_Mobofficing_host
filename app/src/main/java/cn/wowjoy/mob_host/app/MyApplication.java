package cn.wowjoy.mob_host.app;

import com.qihoo360.replugin.RePluginApplication;

import java.io.File;

import static android.os.Environment.getExternalStorageDirectory;


public class MyApplication extends RePluginApplication {

    public static MyApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        File f = new File(getExternalStorageDirectory().getAbsolutePath() + "/wowjoy/");
        if (!f.exists())
            f.mkdirs();
//        CrashHandler catchHandler = CrashHandler.getInstance();
//        catchHandler.init(this);
    }

}
