package cn.wowjoy.mob_host.base;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;


public  class BaseRepository {
    private CompositeDisposable mCompositeDisposable;

    private void onCreate() {
        this.mCompositeDisposable = new CompositeDisposable();
    }

    // 可以终止网络请求
    public void onDestroy() {
        if (null != mCompositeDisposable) {
            this.mCompositeDisposable.clear();
            this.mCompositeDisposable = null;
        }
    }


    protected void addDisposable(Disposable disposable) {
        if (null != mCompositeDisposable) {
            mCompositeDisposable.add(disposable);
        }
    }
}
