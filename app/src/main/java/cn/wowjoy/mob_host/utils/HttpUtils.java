package cn.wowjoy.mob_host.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import cn.wowjoy.mob_host.BuildConfig;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpUtils {
    private Gson gson;
    private static HttpUtils instance;
    private Object mGetWayServer;
    private Object mDownloadServer;


    public static HttpUtils getInstance() {
        if (instance == null) {
            synchronized (HttpUtils.class) {
                if (instance == null) {
                    instance = new HttpUtils();
                }
            }
        }
        return instance;
    }

    public <T> T getGateWayServer(Class<T> t) {
        if (null == mGetWayServer) {
            synchronized (HttpUtils.class) {
                if (null == mGetWayServer) {
                    mGetWayServer = getBuilder(BuildConfig.API_HOST).build().create(t);
                }
            }
        }

        return (T) mGetWayServer;
    }

    public <T> T getDownloadServer(Class<T> t, String url) {
        if (null == mDownloadServer) {
            synchronized (HttpUtils.class) {
                if (null == mDownloadServer) {
                    mDownloadServer = getBuilder(url).build().create(t);
                }
            }
        }

        return (T) mDownloadServer;
    }

    private OkHttpClient getOkHttpClient() {
        OkHttpClient.Builder okBuilder = new OkHttpClient.Builder();
        okBuilder.readTimeout(30, TimeUnit.SECONDS);
        okBuilder.connectTimeout(30, TimeUnit.SECONDS);
        okBuilder.writeTimeout(30, TimeUnit.SECONDS);
//        okBuilder.addInterceptor(new HttpHeadInterceptor());
//        okBuilder.addInterceptor(getInterceptor());
        return okBuilder.build();

    }

    private HttpLoggingInterceptor getInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY); // 测试
        } else {
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE); // 打包
        }
        return interceptor;
    }

    private Retrofit.Builder getBuilder(String apiUrl) {
        Retrofit.Builder builder = new Retrofit.Builder();
        builder.client(getOkHttpClient());
        builder.baseUrl(apiUrl);//设置远程地址
        builder.addConverterFactory(GsonConverterFactory.create(getGson()));
        builder.addCallAdapterFactory(RxJava2CallAdapterFactory.create());
        return builder;
    }

    private Gson getGson() {
        if (gson == null) {
            gson = new GsonBuilder().create();
        }
        return gson;
    }

    private class HttpHeadInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            Request.Builder builder = request.newBuilder();
            builder.header("Content-Type", "application/json;charset=UTF-8");
            builder.addHeader("Connection", "close");
            return chain.proceed(builder.build());
        }
    }


}
