package cn.wowjoy.mob_host.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import cn.wowjoy.mob_host.R;


public class MDialog extends Dialog {

    private String mTitle = null;
    private String mHintStr = null;
    private String mBottonL = null;
    private String mBottonR = null;
    private String mEdit0 = null;
    private String mEdit1 = null;
    private TextView mTileTextView;
    private TextView mHintTextView;
    private EditText mLoginUrl;
    private EditText mApiUrl;
    private View.OnClickListener mCancleClickListener = null;
    private View.OnClickListener mConfirmClickListener = null;

    private int type;
    private Button mCancelBtn = null;
    private Button mConfirmBtn = null;
    private List<String> data;
    private ViewDataBinding vb;
    private LayoutInflater inflater;
    private Activity context;
    public OnListClickedInterface mACInterface;

    public MDialog(Activity context, int theme) {
        super(context, theme);
    }

    public MDialog(Activity context, int theme, String title, String hint, String BottonL, String BottonR, View.OnClickListener cancelBtnClickListener, View.OnClickListener confirmBtnClickListener) {
        super(context, theme);
        mTitle = title;
        mHintStr = hint;
        mBottonL = BottonL;
        mBottonR = BottonR;
        mCancleClickListener = cancelBtnClickListener;
        mConfirmClickListener = confirmBtnClickListener;
    }

    public MDialog(Activity context, int theme, String title, String hint, int type, View.OnClickListener cancelBtnClickListener, String cancelText) {
        super(context, theme);
        this.context = context;
        mCancleClickListener = cancelBtnClickListener;
        mBottonL = cancelText;
        mTitle = title;
        mHintStr = hint;
        this.type = type;
    }

    public MDialog(Activity context, int theme, String hint, String BottonL, String BottonR, View.OnClickListener cancelBtnClickListener, View.OnClickListener confirmBtnClickListener) {
        super(context, theme);
        mHintStr = hint;
        mBottonL = BottonL;
        mBottonR = BottonR;
        mCancleClickListener = cancelBtnClickListener;
        mConfirmClickListener = confirmBtnClickListener;
    }

    public MDialog(Activity context, int theme, String edit0, String edit1) {
        super(context, theme);
        mEdit0 = edit0;
        mEdit1 = edit1;
    }

    public MDialog(Activity context, int theme, List<String> data) {
        super(context, theme);
        this.data = data;
        this.context = context;
        if (inflater == null)
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
            setContentView(R.layout.dialog_waiting);
    }

    public TextView getmTextView() {
        return mHintTextView;
    }



    public void OnListClickItem(OnListClickedInterface onListClickedInterface) {
        mACInterface = onListClickedInterface;
    }


    public interface OnListClickedInterface {
        void OnListClicked(String ai);
    }

    public interface OnConfirmListener {
        void onConfirm();
    }

    private OnConfirmListener onConfirmListener;

    public void setOnConfirmListener(OnConfirmListener onConfirmListener) {
        this.onConfirmListener = onConfirmListener;
    }
}
