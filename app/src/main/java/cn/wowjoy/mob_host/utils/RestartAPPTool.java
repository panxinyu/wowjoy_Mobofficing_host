package cn.wowjoy.mob_host.utils;

import android.content.Context;



/**
 * Created by Administrator on 2018/3/30.
 */

public class RestartAPPTool {
    /**
     * 重启整个APP
     * @param context
     * @param Delayed 延迟多少毫秒
     */
    private static void restartAPP(Context context, long Delayed){
        /**杀死整个进程**/
        android.os.Process.killProcess(android.os.Process.myPid());
    }
    /***重启整个APP*/
    public static void restartAPP(Context context){
        restartAPP(context,2000);
    }
}
