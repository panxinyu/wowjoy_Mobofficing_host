package cn.wowjoy.mob_host.utils;

import android.content.Context;
import android.os.StatFs;
import android.os.storage.StorageManager;

import java.lang.reflect.Method;

/**
 * Created by SJ on 2016/1/27.
 */
public class SDUtil {

    public static boolean canUseSDCard(long size, String sdPath){
        if (sdPath == null){
            return  false;
        }
        if(getAvailableStore(sdPath)>=size){
            return  true;
        }
        return  false;
    }

    // 获取SD卡路径
    public static String getExternalStoragePath() {
        // 获取SdCard状态
        String state = android.os.Environment.getExternalStorageState();

        // 判断SdCard是否存在并且是可用的

        if (android.os.Environment.MEDIA_MOUNTED.equals(state)) {

            if (android.os.Environment.getExternalStorageDirectory().canWrite()) {

                return android.os.Environment.getExternalStorageDirectory()
                        .getAbsolutePath();

            }

        }
        return null;

    }
    // 获取SD卡路径
    public static boolean getExternalStorageAvailable() {
        // 获取SdCard状态
        String state = android.os.Environment.getExternalStorageState();

        // 判断SdCard是否存在并且是可用的

        if (android.os.Environment.MEDIA_MOUNTED.equals(state)) {

            if (android.os.Environment.getExternalStorageDirectory().canWrite()) {

                return true;

            }

        }
        return false;

    }

    /**
     * 　　* 获取存储卡的剩余容量，单位为字节
     *
     * 　　* @param filePath
     *
     * 　　* @return availableSpare
     *
     */
    public static long getAvailableStore(String filePath) {

        // 取得sdcard文件路径

        StatFs statFs = new StatFs(filePath);

        // 获取block的SIZE

        long blocSize = statFs.getBlockSize();

        // 获取BLOCK数量

        // long totalBlocks = statFs.getBlockCount();

        // 可使用的Block的数量
        long availaBlock = statFs.getAvailableBlocks();

        // long total = totalBlocks * blocSize;

        long availableSpare = availaBlock * blocSize;

        return availableSpare;

    }
    /**
     * 得到所有的存储路径（内部存储+外部存储）
     *
     * @param context
     * @return                   // allSdPaths[0]  内部SD卡   allSdPaths[1]  外部SD卡
     */
    public static String[] getAllSdPaths(Context context) {
        Method mMethodGetPaths = null;
        String[] paths = null;
        //通过调用类的实例mStorageManager的getClass()获取StorageManager类对应的Class对象
        //getMethod("getVolumePaths")返回StorageManager类对应的Class对象的getVolumePaths方法，这里不带参数
        StorageManager mStorageManager = (StorageManager)context
                .getSystemService(context.STORAGE_SERVICE);//storage
        try {
            mMethodGetPaths = mStorageManager.getClass().getMethod("getVolumePaths");
            paths = (String[]) mMethodGetPaths.invoke(mStorageManager);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return paths;
    }
}
