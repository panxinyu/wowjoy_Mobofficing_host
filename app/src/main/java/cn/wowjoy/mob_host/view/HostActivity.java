package cn.wowjoy.mob_host.view;

import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.qihoo360.replugin.RePlugin;

import cn.wowjoy.mob_host.databinding.ActivityMainBinding;
import cn.wowjoy.mob_host.R;
import cn.wowjoy.mob_host.base.BaseActivity;

public class HostActivity extends BaseActivity<ActivityMainBinding, HostViewModel> {
    @Override
    protected void init(Bundle savedInstanceState) {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);

        viewModel.check();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                RePlugin.startActivity(HostActivity.this, RePlugin.createIntent("wowjoyMob",
                        "cn.wowjoy.office.main.MainActivity"));
                finish();
            }
        }, 1000);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected Class getViewModelClass() {
        return HostViewModel.class;
    }

}
