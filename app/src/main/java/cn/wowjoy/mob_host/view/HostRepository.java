package cn.wowjoy.mob_host.view;

import android.util.Log;

import com.qihoo360.replugin.RePlugin;
import com.qihoo360.replugin.model.PluginInfo;

import org.reactivestreams.Publisher;

import cn.wowjoy.mob_host.api.HttpClient;
import cn.wowjoy.mob_host.base.BaseRepository;
import cn.wowjoy.mob_host.pojo.UpdateInfo;
import cn.wowjoy.mob_host.utils.FileUtils;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subscribers.DisposableSubscriber;
import okhttp3.ResponseBody;

import static android.os.Environment.getExternalStorageDirectory;

public class HostRepository extends BaseRepository {

    public void checkUpdate() {
        Disposable disposable = HttpClient.getGateWayService().checkUpdate()
                .flatMap(new Function<UpdateInfo, Publisher<ResponseBody>>() {
                    @Override
                    public Publisher<ResponseBody> apply(UpdateInfo updateInfo) throws Exception {
                        if (updateInfo.getVersionCode() > RePlugin.getPluginVersion("Mob")) {
                            String url;
                            if (updateInfo.getForced() == 0)
                                url = updateInfo.getPluginUrl();
                            else
                                url = updateInfo.getApkUrl();
                            return HttpClient.getDownloadService(url.substring(0, url.lastIndexOf("/") + 1)).downloadapk(url.substring(url.lastIndexOf("/") + 1, url.length()));
                        } else
                            return Flowable.empty();
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribeWith(new DisposableSubscriber<ResponseBody>() {
                    @Override
                    public void onNext(ResponseBody responseBody) {
                        if (FileUtils.writeResponseBodyToDisk(responseBody, getExternalStorageDirectory().getAbsolutePath() + "/wowjoy/wowjoyMob.apk")) {
                            PluginInfo pi = RePlugin.install(getExternalStorageDirectory().getAbsolutePath() + "/wowjoy/wowjoyMob.apk");
                            if (pi != null) {
                                boolean suc = RePlugin.preload(pi);
                                Log.e("download", "" + suc);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable t) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
        addDisposable(disposable);
    }

}
