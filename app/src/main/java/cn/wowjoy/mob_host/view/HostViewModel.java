package cn.wowjoy.mob_host.view;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.support.annotation.NonNull;

public class HostViewModel extends AndroidViewModel {

    private HostRepository mHostRepository;

    public HostViewModel(@NonNull Application application) {
        super(application);
    }

    public void check() {
        if (mHostRepository == null)
            mHostRepository = new HostRepository();
        mHostRepository.checkUpdate();
    }
}
